# Discord Moderation bot

This project is a simple Discord moderation bot as an alternative to MEE6. 

## Features

* Clear messages
* Ban users
* Kick users
* Mute users for a certain period of time
* Personal commands (see later)
* (BETA) Music bot

## Future features

* Levels



## Install

### Requirements

```shell
pip install discord.py
```

### Install 

1. Create a new app on the Devlopper Portal of Discord
2. Create a bot
3. Copy the OAuth link and add it on your server
4. Git clone this repo on your server
5. Change the token inside the file 'token.txt'
6. Run the following command: `python3 bot.py`
7. On your Discord server run `!setup`

There will maybe be a DEB file later

## Commands

| Synthax                       | Description                                      |
| ----------------------------- | ------------------------------------------------ |
| !mute @user *time_in_seconds* | Text mute a user for a certain time in seconds   |
| !ban @user                    | ban a user                                       |
| !kick @user                   | Kick a user                                      |
| !clear *number_of_messages*   | Clear a certain number of message in one channel |
| !play *url*                   | Play a YouTube URL in the current voice channel  |
| !stop                         | Stop the current song                            |

## Create your own commands

There is no setup to create a command. This is how to create one inside of the 'bot.py'.

```python
@bot.command()
async def name(ctx, option):
    await ctx.send(option)
```

### Ping script

```python
@bot.command()
async def ping(ctx):
    await ctx.send('pong')
```

## Restrict a command to a specific role

```python
@bot.command()
@commands.has_role('YourRole')
async def ping(ctx):
    await ctx.send('pong')
```

### Personal message

```python
@bot.command()
async def ping(ctx, msg):
    await ctx.send('You say' + msg)
```

### Restrict to a specific permission

```python
@bot.command()
@commands.has_permissions(administrator=True)
async def ping(ctx):
    await ctx.send('pong')
```

## You want more?

If you want more options you can check out the API of discord.py: https://discordpy.readthedocs.io/en/latest/api.html

If you want to share your snippets, you can create an issue.

If you want a clear app, there is a ping app in the script [basic.py](basic.py).

I'll also upload a discord.py cheatsheet on this repo later.